# Copyright 2011 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=testing-cabal ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require python [ blacklist=2 multiunpack=true ]

SUMMARY="A streaming protocol for test results"
DESCRIPTION="
Subunit is a streaming protocol for test results. The protocol is human
readable and easily generated and parsed. By design all the components of
the protocol conceptually fit into the xUnit TestCase->TestResult interaction.

Subunit comes with command line filters to process a subunit stream and
language bindings for python, C, C++ and shell. Bindings are easy to write
for other languages.
"

LICENCES="|| ( Apache-2.0 BSD-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-cpp/cppunit
        dev-libs/check[>=0.9.4]
        dev-python/iso8601[python_abis:*(-)?]
        dev-python/testtools[>=0.9.34][python_abis:*(-)?]
    test:
        dev-python/fixtures[python_abis:*(-)?]
        dev-python/hypothesis[python_abis:*(-)?]
        dev-python/testscenarios[python_abis:*(-)?]
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-static )

prepare_one_multibuild() {
    autotools_src_prepare
}

install_one_multibuild() {
    default
    python_bytecompile
}


