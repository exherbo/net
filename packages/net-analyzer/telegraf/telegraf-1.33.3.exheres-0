# Copyright 2021-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=influxdata tag=v${PV} ] \
    flag-o-matic \
    openrc-service \
    systemd-service [ systemd_files=[ "${WORK}"/scripts/telegraf.service ] ]

SUMMARY="Plugin-driven server agent for collecting & reporting metrics"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO: need to figure out
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.24.0]
    build+run:
        group/telegraf
        user/telegraf
"

pkg_setup() {
    filter-ldflags -Wl,-O1 -Wl,--as-needed
}

src_unpack() {
    default

    edo pushd "${WORK}"
    esandbox disable_net
    edo go mod download -x
    esandbox enable_net
    edo popd
}

src_prepare() {
    default

    # fix unknown version string
    edo sed \
        -e "s:Version = \"unknown\":Version = \"${PV}\":g" \
        -i internal/internal.go

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/github.com/influxdata
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/influxdata/telegraf

    edo ln -s "${TEMP}"/go/pkg "${WORKBASE}"/build
}

src_compile() {
    GO111MODULE="auto" edo go build \
        -o bin/telegraf \
        -v \
        "./cmd/telegraf"

    edo go run "./cmd/telegraf" config > etc/telegraf.conf
}

src_install() {
    dobin bin/telegraf

    insinto /etc/telegraf
    doins etc/telegraf.conf
    keepdir /etc/telegraf/telegraf.d
    edo chown -R telegraf:telegraf "${IMAGE}"/etc/telegraf
    edo chmod 0640 "${IMAGE}"/etc/telegraf/telegraf.conf
    edo chmod 0750 "${IMAGE}"/etc/telegraf{,/telegraf.d}

    install_systemd_files
    install_openrc_files

    insinto /etc/logrotate.d
    doins etc/logrotate.d/${PN}

    keepdir /var/log/${PN}
    edo chown -R telegraf:telegraf "${IMAGE}"/var/log/${PN}
}

