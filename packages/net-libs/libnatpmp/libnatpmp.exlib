# Copyright 2010 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools blacklist=3 with_opt=true multibuild=false ]

export_exlib_phases src_compile src_install

SUMMARY="A lightweight NAT Portmapping Protocol library for POSIX systems"
DESCRIPTION="
NAT-PMP (NAT Port Mapping protocol) provides a way to do NAT traversal. It is implemented in Apple
products (Airport extreme). libnatpmp is an attempt to make a portable and fully compliant
implementation of the protocol for the client side. It is based on non-blocking sockets, and all
calls of the API are asynchronous. It is therefore very easy to integrate the NAT-PMP code into any
event driven code.
"
BASE_URI="https://miniupnp.tuxfamily.org"
HOMEPAGE="${BASE_URI}/${PN}.html"
DOWNLOADS="${BASE_URI}/files/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES=""

DEFAULT_SRC_INSTALL_PARAMS=(
    HEADERS='natpmp.h natpmp_declspec.h'
    INSTALLPREFIX="${IMAGE}"/usr/$(exhost --target)
)

libnatpmp_src_compile() {
    default

    optionq python && setup-py_src_compile
}

libnatpmp_src_install() {
    default

    optionq python && setup-py_src_install
}

