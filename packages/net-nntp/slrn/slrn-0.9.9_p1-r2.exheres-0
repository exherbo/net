# Copyright 2011 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=${PV/_}
require sourceforge [ pv=${PV/_} suffix="tar.gz" ]

SUMMARY="S-Lang read news"
DESCRIPTION="
slrn ('S-Lang read news') is a newsreader, i.e. a program that accesses a
newsserver to read messages from the Internet News service (also known as
'Usenet'). It runs in console mode on various Unix-like systems (including
Linux), 32-bit Windows, OS/2, BeOS and VMS. Beside the usual features of a
newsreader slrn supports scoring rules to highlight, sort or kill articles based
on information from their header. It is highly customizable, allows free
key-bindings and can easily be extended using the sophisticated S-Lang macro
language. Offline reading is possible by using either slrnpull (shipped with
slrn) or a local newsserver (like leafnode or INN).
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    uudeview [[ description = [ Use the uudeview library for decoding uuencoded articles ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        app-arch/sharutils
        sys-libs/slang
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        uudeview? ( dev-libs/uulib )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-nls --with-slrnpull --with-ssl )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( uudeview )
WORK="${WORKBASE}"/${PN}-${MY_PV}

src_prepare() {
    # strip is bad mmkay?
    edo sed -i \
            -e "s,\(\$(INSTALL) -m 755 \)-s,\1," \
            src/Makefile.in

    default
}

