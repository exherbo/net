# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] cmake
require alternatives

export_exlib_phases src_install

SUMMARY="C implementation of the Git core methods as a library with a solid API"
DESCRIPTION="LibGit2 is a from the ground up implementation of a Git library."
HOMEPAGE+=" https://libgit2.org"

LICENCES="GPL-2 [[ note = [ With linking exception ] ]]"
SLOT="0"
MYOPTIONS="
    kerberos [[ description = [ Support for SPNEGO authentication ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

# Tests needs access to internet and fails playing with git inside build tree
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/pcre2
        net-libs/http-parser[>=2.0.0]
        net-libs/libssh2
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
    run:
        !dev-scm/libgit2:0[<1.0.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Disabling this for now, because there is probably nothing needing
    # it and it links libgit2 statically, so it's not that small.
    -DBUILD_CLI:BOOL=FALSE
    -DBUILD_EXAMPLES:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_FUZZERS:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING="Release"
    -DCMAKE_DISABLE_FIND_PACKAGE_mbedTLS=TRUE
    -DDEBUG_POOL:BOOL=FALSE
    -DDEPRECATE_HARD:BOOL=FALSE
    -DENABLE_REPRODUCIBLE_BUILDS:BOOL=TRUE
    -DENABLE_WERROR:BOOL=FALSE
    -DREGEX_BACKEND:STRING="pcre2"
    -DUSE_BUNDLED_ZLIB:BOOL=FALSE
    -DUSE_LEAK_CHECKER:BOOL=FALSE
    -DUSE_SHA1:STRING="CollisionDetection"
    -DUSE_STANDALONE_FUZZERS:BOOL=FALSE
    -DUSE_HTTPS:STRING="OpenSSL"
    -DUSE_NSEC:BOOL=TRUE
    -DUSE_SHA256="OpenSSL-Dynamic"
    -DUSE_THREADS:BOOL=TRUE
    # We don't seem to have that, also
    # "message(FATAL_ERROR "external/system xdiff is not yet supported")"
    -DUSE_XDIFF:STRING="builtin"
)

if ever at_least 1.8.1 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DUSE_HTTP_PARSER:STRING=http-parser
        -DUSE_SSH:STRING=libssh2
    )
else
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DUSE_HTTP_PARSER:STRING=system
        -DUSE_SSH:BOOL=TRUE
    )
fi

CMAKE_SRC_CONFIGURE_OPTION_USES=( 'kerberos GSSAPI' )
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

libgit2_src_install () {
    local alternatives=() host=$(exhost --target)

    cmake_src_install

    alternatives+=(
        /usr/${host}/include/git2           git2-${SLOT}
        /usr/${host}/include/git2.h         git2-${SLOT}.h
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}

