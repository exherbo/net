# Copyright 2021-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ usr/lib/systemd/system/grafana-server.service ] ]

SUMMARY="Open and composable observability and data visualization platform"
HOMEPAGE="https://grafana.com"
DOWNLOADS="
    listed-only:
        platform:amd64? ( https://dl.grafana.com/oss/release/${PN}_${PV}_amd64.deb )
"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="-* ~amd64"
MYOPTIONS="
    platform: amd64
"

RESTRICT="strip"

DEPENDENCIES="
    build+run:
        group/grafana
        user/grafana
"

WORK=${WORKBASE}

src_unpack() {
    default

    edo tar xf data.tar.gz
}

src_prepare() {
    # disable reporting
    edo sed \
        -e 's:;reporting_enabled = true:reporting_enabled = false:g' \
        -i usr/share/${PN}/conf/sample.ini

    default
}

src_test() {
    :
}

src_install() {
    dobin usr/sbin/${PN}{-cli,-server}

    insinto /etc/default
    doins etc/default/${PN}-server

    insinto /etc/${PN}
    newins usr/share/${PN}/conf/sample.ini ${PN}.ini
    doins usr/share/${PN}/conf/ldap.toml
    doins -r usr/share/${PN}/conf/provisioning

    insinto /usr/share/${PN}
    doins -r usr/share/${PN}/*

    edo chmod 0755 "${IMAGE}"/usr/share/${PN}/bin/${PN}{,-cli,-server}

    install_systemd_files

    keepdir /var/{lib,log}/${PN}
    edo chown grafana:grafana "${IMAGE}"/var/{lib,log}/${PN}
}

