# Copyright 2015-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=pingidentity tag=v${PV} ] autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.12 1.11 1.10 ] ]

SUMMARY="OpenID Connect Relying Party (RP) functionality for Apache HTTPd 2.x"
LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/hiredis[>=0.9.0]
        dev-libs/jansson[>=2.0]
        dev-libs/openssl:=[>=1.0]
        dev-libs/pcre
        net-misc/curl
        www-servers/apache[>=2.2]
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --with-hiredis )

src_prepare() {
    # The evaluation of the neccessary LDFLAGS from pkg-config is broken
    edo sed -i -e "/REDIS_LIBS=/s:=.*:=-lhiredis:" Makefile.in

    # Same for CFLAGS but here it only matters for the tests
    edo sed -i -e '/test\/test.c/s:$(CFLAGS)::' Makefile.in

    # Ensure the .so file gets installed into ${IMAGE}, not the live fs
    edo sed -i -e "/@APXS2@/s:-i:-S LIBEXECDIR=${IMAGE}/usr/$(exhost --target)/libexec/apache2/modules -i:" Makefile.in
    dodir /usr/$(exhost --target)/libexec/apache2/modules

    eautoreconf
}

