# Copyright 2011-2012 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require wafsamba

SUMMARY="Tevent is an event system based on the talloc memory management library"
DESCRIPTION="
Tevent is an event system based on the talloc memory management library. It is the core event system
used in Samba. The low level tevent has support for many event types, including timers, signals, and
the classic file descriptor events. Tevent also provide helpers to deal with asynchronous code
providing the tevent_req (tevent request) functions.
"
HOMEPAGE="https://${PN}.samba.org"

LICENCES="LGPL-3"
SLOT="0"
MYOPTIONS="
    ( libc: musl )
"

DEPENDENCIES="
    build+run:
        dev-libs/libbsd [[ note = [ automagic ] ]]
        dev-libs/talloc[>=2.4.3][python_abis:*(-)?]
        !libc:musl? ( dev-libs/libxcrypt:= )
    build+test:
        dev-util/cmocka[>=1.1.3]
"

WAF_SRC_COMPILE_PARAMS+=( -j1 )

DEFAULT_SRC_INSTALL_EXCLUDE=( release-script.sh )

