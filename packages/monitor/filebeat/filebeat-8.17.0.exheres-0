# Copyright 2021-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Lightweight shipper for forwarding and centralizing log data"
HOMEPAGE="https://www.elastic.co/guide/en/beats/${PN}/current/${PN}-overview.html"
DOWNLOADS="https://artifacts.elastic.co/downloads/beats/${PN}/${PNV}-linux-x86_64.tar.gz"

LICENCES="Elastic-License"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="strip"

DEPENDENCIES=""

WORK="${WORKBASE}/${PNV}-linux-x86_64"

src_test() {
    :
}

src_install() {
    dobin ${PN}

    insinto /etc/${PN}
    doins *.yml
    doins -r modules.d

    insinto /usr/share/${PN}
    doins -r {kibana,module}

    # remove empty directories
    edo find "${IMAGE}" -type d -empty -delete

    keepdir /var/{lib,log}/${PN}

    install_systemd_files

    emagicdocs
}

pkg_postinst() {
    elog "To run the Index setup configure the Elasticsearch host under"
    elog "output.elasticsearch in /etc/filebeat/filebeat.yml and"
    elog "run:"
    elog "filebeat setup \\"
    elog "    -c /etc/filebeat/filebeat.yml \\"
    elog "    --path.home /usr/share/filebeat \\"
    elog "    --path.config /etc/filebeat \\"
    elog "    --path.data /var/lib/filebeat \\"
    elog "    --path.logs /var/log/filebeat"
}

