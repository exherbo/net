# Copyright 2020-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ma1uta release=${PV} pnv=${PN}_${PV}_all suffix=deb ] \
    systemd-service

SUMMARY="ma1sd - Federated Matrix Identity Server"
DESCRIPTION="
ma1sd is a Federated Matrix Identity server for self-hosted Matrix infrastructures with enhanced
features. As an enhanced Identity service, it implements the Identity service API and several extra
features that greatly enhance user experience within Matrix. It is the one stop shop for anything
regarding Authentication, Directory and Identity management in Matrix built in a single coherent
product.

ma1sd is specifically designed to connect to an existing on-premise Identity store (AD/Samba/LDAP,
SQL Database, Web services/app, etc.) and ease the integration of a Matrix infrastructure within an
existing one.

The core principle of ma1sd is to map between Matrix IDs and 3PIDs (Third-Party IDentifiers) for
the Homeserver and its users. 3PIDs can be anything that uniquely and globally identify a user,
like:

* Email address
* Phone number
* Skype/Live ID
* Twitter handle
* Facebook ID
"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
    run:
        virtual/jdk:*
"

WORK=${WORKBASE}

src_unpack() {
    default

    edo tar xf data.tar.xz
}

src_test() {
    :
}

src_install() {
    herebin ${PN} <<EOF
#!/bin/sh
java -jar /usr/$(exhost --target)/lib/${PN}/${PN}.jar "\$@"
EOF

    insinto /etc/${PN}
    newins etc/${PN}/${PN}.example.yaml ${PN}.yaml
    edo chown -R ma1sd:ma1sd "${IMAGE}"/etc/${PN}
    edo chmod 0640 "${IMAGE}"/etc/${PN}/${PN}.yaml

    insinto /usr/$(exhost --target)/lib/${PN}
    doins usr/lib/${PN}/${PN}.jar

    keepdir /var/lib/${PN}
    edo chown -R ma1sd:ma1sd "${IMAGE}"/var/lib/${PN}

    install_systemd_files
}

