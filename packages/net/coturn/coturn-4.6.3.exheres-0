# Copyright 2020-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github systemd-service

SUMMARY="TURN/STUN & ICE Server"
DESCRIPTION="
The TURN Server is a VoIP media traffic NAT traversal server and gateway. It can be used as a
general-purpose network traffic TURN server and gateway, too.
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        group/${PN}
        user/${PN}
        dev-libs/libevent:=
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --examplesdir=/usr/share/doc/${PNVR}/examples
    --mandir=/usr/share
    --schemadir=/usr/share/${PN}
    --turndbdir=/var/lib/${PN}
)
DEFAULT_SRC_COMPILE_PARAMS=( ARCHIVERCMD="${AR} -r" )

src_prepare() {
    edo sed \
        -e 's:#log-file=/var/tmp/turn.log:log-file=/var/log/coturn/turnserver.log:' \
        -i examples/etc/turnserver.conf

    default
}

src_configure() {
    export PKGCONFIG="${PKG_CONFIG}"

    export TURN_NO_HIREDIS=yes
    export TURN_NO_MONGO=yes
    export TURN_NO_MYSQL=yes
    export TURN_NO_PQ=yes
    export TURN_NO_PROMETHEUS=yes
    export TURN_NO_SQLITE=yes
    ! option systemd && export TURN_NO_SYSTEMD=yes

    default
}

src_install() {
    default

    dodir /etc/${PN}
    edo mv "${IMAGE}"/etc/turnserver.conf.default "${IMAGE}"/etc/${PN}/turnserver.conf

    keepdir /var/{lib,log}/${PN}
    edo chown -R coturn:coturn "${IMAGE}"/var/{lib,log}/${PN}

    install_systemd_files

    insinto "${SYSTEMDTMPFILESDIR}"
    hereins ${PN}.conf <<EOF
d /run/coturn 0750 coturn coturn
EOF
}

