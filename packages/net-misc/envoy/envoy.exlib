# Copyright 2014 Vasiliy Tolstov <v.tolstov@selfip.ru>
# Copyright 2014-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=vodik tag=v${PV} ] pam systemd-service zsh-completion

export_exlib_phases src_prepare src_install

SUMMARY="Wrapper around ssh/gpg agent that works with systemd and cgroups"
LICENCES="GPL-3"
SLOT="0"

MYOPTIONS=""
DEPENDENCIES="
    build:
        dev-util/ragel
    build+run:
        sys-apps/dbus
        sys-apps/systemd
        sys-libs/pam
    run:
        app-crypt/gnupg
        net-misc/openssh
"

envoy_src_prepare() {
    default

    edo sed -i -e "s:libsystemd-daemon:libsystemd:" Makefile
    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)pkg-config:" Makefile
    edo sed -i -e "s:/usr/bin:/usr/$(exhost --target)/bin:" Makefile
    edo sed -i -e "s:/usr/lib/systemd/system:${SYSTEMDSYSTEMUNITDIR}:" Makefile
    edo sed -i -e "s:/usr/lib/systemd/user:${SYSTEMDUSERUNITDIR}:" Makefile
    edo sed -i -e "s:\${LIBDIR}/security:$(getpam_mod_dir):" Makefile
}

envoy_src_install() {
    default

    dozshcompletion zsh-completion _${PN}

    if ! option zsh-completion ; then
        edo rm -r "${IMAGE}"/usr/share/zsh
    fi

    pamd_mimic_system ${PN} auth session
}

