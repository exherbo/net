# Copyright 2009 Mike Kelly
# Copyright 2013 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.17 ] ]
require github [ user=traviscross tag="v${PV}" ]

SUMMARY="an improved traceroute / ping"
DESCRIPTION="
mtr combines the functionality of the 'traceroute' and 'ping' programs
in a single network diagnostic tool.

As mtr starts, it investigates the network connection between the host
mtr runs on and a user-specified destination host. After it determines
the address of each network hop between the machines, it sends a
sequence ICMP ECHO requests to each one to determine the quality of the
link to each machine. As it does this, it prints running statistics
about each machine.
"
HOMEPAGE="https://www.bitwizard.nl/${PN}/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk
    json [[ description = [ JSON output format support ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        sys-libs/libcap
        sys-libs/ncurses
        gtk? ( x11-libs/gtk+:3 )
        json? ( dev-libs/jansson )
    test:
        dev-lang/python:*
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-ipv6 )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gtk
    'json jansson'
)

# many tests require running as root
RESTRICT="test"

pkg_postinst() {
    # make install sets 'cap_net_raw+ep' but we can't rely on paludis preserving that
    # (because e.g. binaries)
    nonfatal edo setcap cap_net_raw+ep  "${ROOT}"/usr/$(exhost --target)/bin/${PN}-packet \
        ||   edo chmod u+s              "${ROOT}"/usr/$(exhost --target)/bin/${PN}-packet
}

