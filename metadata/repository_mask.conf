(
    dev-libs/tevent[~scm]
    media-video/rtmpdump[~scm]
    net-analyzer/fail2ban[~scm]
    net-im/bitlbee[~scm]
    net-im/bitlbee-steam[~scm]
    net-irc/weechat[~scm]
    net-irc/znc[~scm]
    net-libs/jreen[~scm]
    net-libs/miniupnpc[~scm]
    net-misc/mosh[~scm]
    net-p2p/transmission[~scm]
    net-proxy/torsocks[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

net-fs/cifs-utils[<7.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Jan 2023 ]
    token = security
    description = [ CVE-2020-14342, CVE-2021-20208, CVE-2022-27239, CVE-2022-29869 ]
]]

web-apps/cgit[<0.9.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 May 2013 ]
    token = security
    description = [ CVE-2013-2117 ]
]]

net-libs/libssh[<0.10.6] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Dec 2023 ]
    token = security
    description = [ CVE-2023-6004, CVE-2023-6918, CVE-2023-48795 ]
]]

www-servers/nginx[<1.26.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Feb 2025 ]
    token = security
    description = [ CVE-2025-23419 ]
]]

net-libs/libmicrohttpd[<0.9.76] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Apr 2023 ]
    token = security
    description = [ CVE-2023-27371 ]
]]

net-proxy/torsocks[<2] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 19 Jan 2013 ]
    token = security
    description = [ See https://lists.torproject.org/pipermail/tor-dev/2013-June/004959.html ]
]]

(
    net-remote/FreeRDP[<2.11.7]
    net-remote/FreeRDP[>=3.0.0&<3.5.1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 22 Apr 2024 ]
    *token = security
    *description = [ CVE-2024-320{39..41}, CVE-2024-324{58..60} ]
]]

app-crypt/krb5[<1.21.3-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Mar 2025 ]
    token = security
    description = [ CVE-2025-24528 ]
]]

net-proxy/squid[<5.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jul 2022 ]
    token = security
    description = [ CVE-2021-46784 ]
]]

net-misc/openvpn[<2.6.12] [[
    author = [ Tom Briden <tom@decompile.me.uk> ]
    date = [ 05 Aug 2024 ]
    token = security
    description = [ CVE-2024-5594 ]
]]

net-analyzer/tcpdump[<4.99.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Sep 2024 ]
    token = security
    description = [ CVE-2024-2397 ]
]]

net-analyzer/wireshark[<4.4.4] [[
    author = [ David Legrand <david.legrand@clever-cloud.com> ]
    date = [ 20 Feb 2025 ]
    token = security
    description = [ CVE-2025-1492 ]
]]

net/net-snmp[<5.9.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 Dec 2022 ]
    token = security
    description = [ CVE-2022-248{05..10} ]
]]

web-apps/cgit[<0.12] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 14 Jan 2015 ]
    token = security
    description = [ CVE-2016-1899, CVE-2016-1900, CVE-2016-1901 ]
]]

net-misc/socat[<1.7.3.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Feb 2015 ]
    token = security
    description = [ http://www.dest-unreach.org/socat/contrib/socat-secadv7.html
                    http://www.dest-unreach.org/socat/contrib/socat-secadv8.html ]
]]

(
    dev-db/mariadb[<10.5.13]
    dev-db/mariadb[>=10.6&<10.6.16]
    dev-db/mariadb[>=10.11&<10.11.6]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 16 Nov 2023 ]
    *token = security
    *description = [ CVE-2023-22084 ]
]]

net-fs/samba[<4.19.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Nov 2023 ]
    token = security
    description = [ CVE-2018-14628 ]
]]

net-mail/dovecot[<2.3.21.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Aug 2024 ]
    token = security
    description = [ CVE-2024-23184, CVE-2024-23185 ]
]]

www-servers/apache[<2.4.62] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jul 2024 ]
    token = security
    description = [ CVE-2024-40725, CVE-2024-40898 ]
]]

net-irc/weechat[<1.9.1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 30 Sep 2017 ]
    token = security
    description = [ CVE-2017-14727 ]
]]

media-video/rtmpdump[<2.4_p20151223] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2015-827{0,1,2} ]
]]

net-mail/tnef[<1.4.18] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Oct 2020 ]
    token = security
    description = [ CVE-2019-18849 ]
]]

net-wireless/hostapd[<2.10] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 Dec 2022 ]
    token = security
    description = [ CVE-2021-30004, CVE-2022-23303, CVE-2022-23304 ]
]]

net-remote/teamviewer[<15.51.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Feb 2024 ]
    token = security
    description = [ CVE-2024-0819 ]
]]

sys-auth/sssd[<2.9.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 May 2024 ]
    token = security
    description = [ CVE-2023-3758 ]
]]

net/mosquitto[<2.0.17] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Sep 2023 ]
    token = security
    description = [ CVE-2023-0809, CVE-2023-3592, CVE-2023-28366 ]
]]

net-libs/nghttp2[<1.41.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2020 ]
    token = security
    description = [ CVE-2020-11080 ]
]]

net-irc/znc[<1.9.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 03 Jul 2024 ]
    token = security
    description = [ CVE-2024-39844 ]
]]

net-p2p/transmission[<2.94] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Jul 2018 ]
    token = security
    description = [ CVE-2018-5702 ]
]]

net-analyzer/ettercap[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jan 2019 ]
    token = security
    description = [ CVE-2014-{6395,6396,9376,9377,9378,9379,9380,9381}, CVE-2017-6430 ]
]]

net/synapse[<1.120.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Dec 2024 ]
    token = security
    description = [ CVE-2024-{37302,37303,52805,52815,53863,53867} ]
]]

net-libs/zeromq[<4.3.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Sep 2020 ]
    token = security
    description = [ CVE-2020-15166 ]
]]

net/gitea[<1.16.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Apr 2022 ]
    token = security
    description = [ CVE-2022-1058 ]
]]

net/gogs[<0.11.91] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Nov 2019 ]
    token = security
    description = [ CVE-2019-14544 ]
]]

www-servers/lighttpd[<1.4.69] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Feb 2023 ]
    token = security
    description = [ CVE-2022-37797, CVE-2022-41556 ]
]]

sys-cluster/ceph[<14.2.20] [[
    author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    date = [ 15 May 2021 ]
    token = security
    description = [ CVE-2021-3509, CVE-2021-3524, CVE-2021-3531 ]
]]

(
    dev-db/redis[<7.2.7]
    dev-db/redis[>=7.4.0&<7.4.2]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 08 Jan 2025 ]
    *token = security
    *description = [ CVE-2024-46981, CVE-2024-51741 ]
]]

www-servers/tomcat-bin[<9.0.83] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 06 Dec 2023 ]
    token = security
    description = [ CVE-2023-46589 ]
]]

net/coturn[<4.5.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Jan 2021 ]
    token = security
    description = [ CVE-2020-26262 ]
]]

web-apps/mailman[<2.1.35] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 20 Oct 2021 ]
    token = security
    description = [ CVE-2021-42096, CVE-2021-42097 ]
]]

net/solr[<8.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Jun 2021 ]
    token = security
    description = [ CVE-2021-27905, CVE-2021-29262, CVE-2021-29943 ]
]]

net-libs/libmaxminddb[<1.4.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Nov 2020 ]
    token = security
    description = [ CVE-2020-28241 ]
]]

net-mail/dovecot-pigeonhole[<0.5.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Jun 2021 ]
    token = security
    description = [ CVE-2020-28200 ]
]]

web-apps/mailman[>=3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 Oct 2021 ]
    token = broken
    description = [ Needs testing, also misses postorious and hyperkitty for
                    web interface and archives ]
]]

web-apps/grafana[<11.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Nov 2024 ]
    token = security
    description = [ CVE-2024-9476 ]
]]

web-apps/opensearch[<2.11.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Oct 2023 ]
    token = security
    description = [ CVE-2023-45807 ]
]]

net/openhab[<4.2.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Aug 2024 ]
    token = security
    description = [ CVE-2024-424{67..70} ]
]]

web-apps/opensearch-logstash[<7.16.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Feb 2022 ]
    token = security
    description = [ CVE-2021-44832 ]
]]

net/solr[<8.11.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Dec 2021 ]
    token = security
    description = [ CVE-2021-44228 ]
]]

net/keycloak[<26.0.8] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Jan 2025 ]
    token = security
    description = [ CVE-2024-11734, CVE-2024-11736 ]
]]

dev-libs/hiredis[<1.0.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2022 ]
    token = security
    description = [ CVE-2021-32765 ]
]]

web-apps/opensearch-dashboards[<2.19.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Feb 2025 ]
    token = security
    description = [ CVE-2024-21538 ]
]]

dev-scm/libgit2:1.6[<1.6.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Mar 2024 ]
    token = security
    description = [ CVE-2024-24577 ]
]]

net-misc/telnet[<2.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 13 Nov 2022 ]
    token = security
    description = [ CVE-2019-0053, CVE-2022-39028 ]
]]

monitor/prometheus[<2.41.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Dec 2022 ]
    token = security
    description = [ CVE-2022-46146 ]
]]

net-misc/connman[<1.42] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Sep 2023 ]
    token = security
    description = [ CVE-2023-28488 ]
]]

net-proxy/privoxy[<3.0.34] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Nov 2023 ]
    token = security
    description = [ CVE-2021-4454{0,1,2,3} ]
]]

monitor/loki[<2.9.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Nov 2023 ]
    token = security
    description = [ CVE-2023-39325, CVE-2023-44487 ]
]]

web-apps/wiki-js[<2.5.304] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Sep 2024 ]
    token = security
    description = [ CVE-2024-45298 ]
]]

dev-db/unixODBC[<2.3.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 May 2024 ]
    token = security
    description = [ CVE-2024-1013 ]
]]

sys-apps/cockpit[<322] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Aug 2024 ]
    token = security
    description = [ CVE-2024-6126 ]
]]

dev-db/valkey[<8.0.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 08 Jan 2025 ]
    token = security
    description = [ CVE-2024-46981, CVE-2024-51741 ]
]]

app-misc/oath-toolkit[<2.6.12] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 Nov 2024 ]
    token = security
    description = [ CVE-2024-47191 ]
]]

net/opentofu[<1.8.8] [[
    author = [ David Legrand <david.legrand@clever-cloud.com> ]
    date = [ 27 Dec 2024 ]
    token = security
    description = [ CVE-2024-45337, CVE-2024-45338 ]
]]

net-analyzer/iperf[<3.18] [[
    author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    date = [ 13 Jan 2025 ]
    token = security
    description = [ CVE-2024-53580 ]
]]
