Title: ldb-removal
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2024-09-05
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-db/ldb

net-fs/samba[>=4.21.0] now includes dev-db/ldb and upstream doesn't provide a standalone
version anymore.

All dependants have been migrated, to update run:

# cave resolve --reinstall-dependents-of dev-db/ldb nothing --permit-uninstall 'dev-db/ldb'
